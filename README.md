# FAPPA
(Famille d’Accueil Pour Personnes Agées)

#### Environnement : 

- Docker 
- PHP 7.4
- PostgreSQL
- NodeJs
- NPM

#### Outils : 

- PGAdmin -> Client pour la Base de données 
- MailDev -> Pour la gestion des mail

## Pré-requis outils

- Docker >= 20.10.8
- Compose => 2.0.0

Vous pouvez verifier les pre-requis avec la commande : 

```bash
php bin/console check:requirements
```

## Developement Informations

### Local environment

| Services    | Ports | URL                   |
| ----------- | ----- | --------------------- |
| PGAdmin     | 5050  | http://localhost:5050 |
| MailDev     | 8081  | http://localhost:8081 |
| Web         | 8000  | http://localhost:8000 |

### Users Account

| Email               | Passwonrd | Type              | Services |
| ------------------- | --------- | ----------------- | -------- |
| admin@gmail.com     | root      | Admin account     | PGAdmin  |

### Installation

#### New Install 

1. Run : `./dockerkit install`

#### Already installed

Launch : `./dockerkit up`

#### Connexion Informations

DB :

- DB_NAME : `fappa`
- Host: `db_fappa`
- Username : `postgres`
- Password : `postgres`

#### Lancer les tests

```bash
php bin/phpunit --testdox
```

#### Utiliser Php CS fixer (normes PSR-2)

```bash
tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src
```

#### Envoi des mails en attente

Les mails de prise de contact sont stockés en BDD, lancé la commande pour les confirmer et les envoyés

```bash
php bin/console app:send-contact
```