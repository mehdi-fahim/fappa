<?php

namespace App\Repository;

use App\Entity\Annonce;
use App\Entity\Categorie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Annonce|null find($id, $lockMode = null, $lockVersion = null)
 * @method Annonce|null findOneBy(array $criteria, array $orderBy = null)
 * @method Annonce[]    findAll()
 * @method Annonce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Annonce::class);
    }

    /**
     * @return Annonce[] Returns an array of Annonce objects of 2 last
     */
    public function lastTwoAnnonce()
    {
        return $this->createQueryBuilder('p')
                    ->orderBy('p.id', 'DESC')
                    ->setMaxResults(2)
                    ->getQuery()
                    ->getResult()
        ;
    }

    /**
     * @return Annonce[] Returns an array of Annonce objects of 2 last
     */
    public function findAllAnnonceByCategorie(Categorie $categorie)
    {
        return $this->createQueryBuilder('p')
                    ->where(':categorie MEMBER OF p.categorie')
                    ->andWhere('p.is_published = TRUE')
                    ->setParameter('categorie', $categorie)
                    ->getQuery()
                    ->getResult()
        ;
    }
}
