<?php

namespace App\Controller;

use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfilController extends AbstractController
{
    /**
     * @Route("/profil", name="profil")
     */
    public function index(UsersRepository $userRepository): Response
    {
        return $this->render('profil/index.html.twig', [
            'user_profil_host' => 'Aucun Profil',
        ]);
    }
}
