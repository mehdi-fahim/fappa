<?php

namespace App\Controller;

use App\Repository\AnnonceRepository;
use App\Entity\Annonce;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnnonceController extends AbstractController
{
    /**
     * @Route("/annonces", name="annonces")
     */
    public function index(AnnonceRepository $annoneRepo, PaginatorInterface $paginator, Request $request): Response
    {
        $data = $annoneRepo->findAll();

        $annonces = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('annonces/index.html.twig', [
            'annonces' => $annonces,
        ]);
    }

    /**
     * @Route("/annonce/{slug}", name="annonce_details")
     */
    public function details(Annonce $annonce): Response
    {
        return $this->render('annonces/details.html.twig', [
            'annonce' => $annonce
        ]);
    }
}
