<?php

namespace App\Controller;

use App\Repository\CategorieRepository;
use App\Repository\AnnonceRepository;
use App\Entity\Categorie;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    /**
     * @Route("/categorie", name="categorie")
     */
    public function index(CategorieRepository $categorieRepository): Response
    {
        return $this->render('categorie/index.html.twig', [
            'categories' => $categorieRepository->findAll(),
        ]);
    }

    /**
     * @Route("/categorie/{slug}", name="categorie_annonce")
     */
    public function getCate(Categorie $cat, AnnonceRepository $annRepo, PaginatorInterface $p, Request $req): Response
    {
        $data = $annRepo->findAllAnnonceByCategorie($cat);

        $annonces = $p->paginate(
            $data,
            $req->query->getInt('page', 1),
            6
        );

        return $this->render('categorie/categorie_detail.html.twig', [
            'categorie' => $cat,
            'annonces' => $annonces,
        ]);
    }
}
