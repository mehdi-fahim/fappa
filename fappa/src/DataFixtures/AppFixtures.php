<?php

namespace App\DataFixtures;

use App\Entity\Users;
use App\Entity\Annonce;
use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // Utilisation de Faker
        $faker = Factory::create('fr_FR');

        // Création d'un utilisateur
        $user = new Users();

        $user->setNom($faker->lastName())
             ->setPrenom($faker->firstName())
             ->setEmail("user@gmail.com")
             ->setTelephone($faker->phoneNumber());

        $password = $this->encoder->encodePassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);

        // Création de 1 catégories aléatoire
        for ($i = 0; $i < 1; $i++) {
            $categorie = new Categorie();

            $categorie->setNom("Enfants")
                    ->setSlug("enfant")
                    ->setDescription("Si vous souhaitez acceuillir un enfant chez vous.");

            $manager->persist($categorie);

            // Création de 5 Annonces/categorie aléatoire
            for ($j = 0; $j < 5; $j++) {
                $annonce = new Annonce();

                $annonce->setTitre($faker->words(3, true))
                        ->setSlug($faker->slug())
                        ->setDescription($faker->paragraph(3, true))
                        ->setAdresse($faker->address())
                        ->setCodePostale($faker->postCode())
                        ->setVille($faker->city())
                        ->setPrix($faker->randomfloat(2, 2000, 5000))
                        ->setImage("https://source.unsplash.com/random/700x400")
                        ->setIsPublished($faker->boolean(50))
                        ->addCategorie($categorie)
                        ->setUserInfo($user);

                $manager->persist($annonce);
            }
        }

        // Création de 5 Annonces/categorie aléatoire
        for ($i = 0; $i < 1; $i++) {
            $categorie = new Categorie();

            $categorie->setNom("Personne Agée")
                    ->setSlug("personne-agee")
                    ->setDescription("Si vous souhaitez acceuillir une personne agée chez vous.");

            $manager->persist($categorie);

            // Création de 5 Annonces aléatoire
            for ($j = 0; $j < 5; $j++) {
                $annonce = new Annonce();

                $annonce->setTitre($faker->words(3, true))
                        ->setSlug($faker->slug())
                        ->setDescription($faker->paragraph(3, true))
                        ->setAdresse($faker->address())
                        ->setCodePostale($faker->postCode())
                        ->setVille($faker->city())
                        ->setPrix($faker->randomfloat(2, 2000, 5000))
                        ->setImage("https://source.unsplash.com/random/600x400")
                        ->setIsPublished($faker->boolean(50))
                        ->addCategorie($categorie)
                        ->setUserInfo($user);

                $manager->persist($annonce);
            }
        }

        // Création de 5 Annonces/categorie aléatoire
        for ($i = 0; $i < 1; $i++) {
            $categorie = new Categorie();

            $categorie->setNom("Personne Handicapé")
                    ->setSlug("personne-handicapé")
                    ->setDescription("Si vous souhaitez acceuillir une personne handicapé chez vous.");

            $manager->persist($categorie);

            // Création de 5 Annonces aléatoire
            for ($j = 0; $j < 5; $j++) {
                $annonce = new Annonce();

                $annonce->setTitre($faker->words(3, true))
                        ->setSlug($faker->slug())
                        ->setDescription($faker->paragraph(3, true))
                        ->setAdresse($faker->address())
                        ->setCodePostale($faker->postCode())
                        ->setVille($faker->city())
                        ->setPrix($faker->randomfloat(2, 2000, 5000))
                        ->setImage("https://source.unsplash.com/random/600x400")
                        ->setIsPublished($faker->boolean(50))
                        ->addCategorie($categorie)
                        ->setUserInfo($user);

                $manager->persist($annonce);
            }
        }



        $manager->flush();
    }
}
