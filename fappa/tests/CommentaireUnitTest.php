<?php

namespace App\Tests;

use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $commentaire = new Commentaire();

        $commentaire->setContenu("Ce contenu est true");

        $this->assertTrue($commentaire->getContenu()  === "Ce contenu est true");
    }

    public function testIsFalse(): void
    {
        $commentaire = new Commentaire();

        $commentaire->setContenu("Ce contenu est true");

        $this->assertFalse($commentaire->getContenu()  === "Ce contenu est false");
    }

    public function testIsEmpty(): void
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getContenu());
    }
}
