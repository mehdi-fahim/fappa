<?php

namespace App\Tests;

use App\Entity\Categorie;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $categorie = new Categorie();

        $categorie->setNom("True")
                  ->setSlug("true")
                  ->setDescription("Cette description est true");

        $this->assertTrue($categorie->getNom() === "True");
        $this->assertTrue($categorie->getSlug()  === "true");
        $this->assertTrue($categorie->getDescription()  === "Cette description est true");
    }

    public function testIsFalse(): void
    {
        $categorie = new Categorie();

        $categorie->setNom("True")
                  ->setSlug("true")
                  ->setDescription("Cette description est true");

        $this->assertFalse($categorie->getNom() === "False");
        $this->assertFalse($categorie->getSlug()  === "false");
        $this->assertFalse($categorie->getDescription()  === "Cette description est false");
    }

    public function testIsEmpty(): void
    {
        $categorie = new Categorie();

        $this->assertEmpty($categorie->getNom());
        $this->assertEmpty($categorie->getSlug());
        $this->assertEmpty($categorie->getDescription());
    }
}
