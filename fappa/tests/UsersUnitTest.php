<?php

namespace App\Tests;

use App\Entity\Users;
use PHPUnit\Framework\TestCase;

class UsersUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new Users();

        $user->setNom("true")
             ->setPrenom("true")
             ->setEmail("true@gmail.com")
             ->setPassword("secretpasswordtrue")
             ->setTelephone("0123456789");

             $this->assertTrue($user->getNom() === "true");
             $this->assertTrue($user->getPrenom()  === "true");
             $this->assertTrue($user->getEmail()  === "true@gmail.com");
             $this->assertTrue($user->getPassword()  === "secretpasswordtrue");
             $this->assertTrue($user->getTelephone()  === "0123456789");
    }

    public function testIsFalse(): void
    {
        $user = new Users();

        $user->setNom("true")
             ->setPrenom("true")
             ->setEmail("true@gmail.com")
             ->setPassword("secretpasswordtrue")
             ->setTelephone("0123456789");

            $this->assertFalse($user->getNom() === "false");
            $this->assertFalse($user->getPrenom()  === "false");
            $this->assertFalse($user->getEmail()  === "false@gmail.com");
            $this->assertFalse($user->getPassword()  === "secretpasswordfalse");
            $this->assertFalse($user->getTelephone()  === "0000000000");
    }

    public function testIsEmpty(): void
    {
        $user = new Users();

        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getTelephone());
    }
}
