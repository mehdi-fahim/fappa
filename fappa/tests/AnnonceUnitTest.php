<?php

namespace App\Tests;

use App\Entity\Annonce;
use PHPUnit\Framework\TestCase;

class AnnonceUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $annonce = new Annonce();

        $annonce->setTitre("true")
             ->setSlug("true")
             ->setDescription("Ceci est une description true")
             ->setAdresse("123 rue des Plouc")
             ->setCodePostale("93000")
             ->setVille("Trueville")
             ->setPrix(1860.20)
             ->setImage("https://www.imagetrue.fr")
             ->setIsPublished(true);

             $this->assertTrue($annonce->getTitre() === "true");
             $this->assertTrue($annonce->getSlug()  === "true");
             $this->assertTrue($annonce->getDescription()  === "Ceci est une description true");
             $this->assertTrue($annonce->getAdresse()  === "123 rue des Plouc");
             $this->assertTrue($annonce->getCodePostale()  === "93000");
             $this->assertTrue($annonce->getVille()  === "Trueville");
             $this->assertTrue($annonce->getPrix()  == 1860.20);
             $this->assertTrue($annonce->getImage()  === "https://www.imagetrue.fr");
             $this->assertTrue($annonce->getIsPublished()  === true);
    }

    public function testIsFalse(): void
    {
        $annonce = new Annonce();

        $annonce->setTitre("true")
             ->setSlug("true")
             ->setDescription("Ceci est une description true")
             ->setAdresse("123 rue des Plouc")
             ->setCodePostale("93000")
             ->setVille("Trueville")
             ->setPrix(1860.20)
             ->setImage("https://www.imagetrue.fr")
             ->setIsPublished(true);

             $this->assertFalse($annonce->getTitre() === "false");
             $this->assertFalse($annonce->getSlug()  === "false");
             $this->assertFalse($annonce->getDescription()  === "Ceci est une description false");
             $this->assertFalse($annonce->getAdresse()  === "123 rue des Plouc Ploc");
             $this->assertFalse($annonce->getCodePostale()  === "91000");
             $this->assertFalse($annonce->getVille()  === "Falseville");
             $this->assertFalse($annonce->getPrix()  == 2060.20);
             $this->assertFalse($annonce->getImage()  === "https://www.imagefalse.fr");
             $this->assertFalse($annonce->getIsPublished()  === false);
    }

    public function testIsEmpty(): void
    {
        $annonce = new Annonce();

        $this->assertEmpty($annonce->getTitre());
        $this->assertEmpty($annonce->getSlug());
        $this->assertEmpty($annonce->getDescription());
        $this->assertEmpty($annonce->getAdresse());
        $this->assertEmpty($annonce->getCodePostale());
        $this->assertEmpty($annonce->getVille());
        $this->assertEmpty($annonce->getPrix());
        $this->assertEmpty($annonce->getImage());
        $this->assertEmpty($annonce->getIsPublished());
    }
}
