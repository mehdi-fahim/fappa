<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211116103016 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update de l\'entité USER';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE user_id_seq CASCADE');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('ALTER TABLE users ADD nom VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE users ADD prenom VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE users ADD date_update TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE users ADD telephone VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE users DROP name');
        $this->addSql('ALTER TABLE users DROP lastname');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, name VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, date_create TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE users ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE users ADD lastname VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE users DROP nom');
        $this->addSql('ALTER TABLE users DROP prenom');
        $this->addSql('ALTER TABLE users DROP date_update');
        $this->addSql('ALTER TABLE users DROP telephone');
    }
}
